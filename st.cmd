#!/usr/bin/env iocsh.bash

# Required modules
## module_name,version

require(raritanpdu5260r)
# Environment variables
#
## Database macros
#epicsEnvSet(P, "LabS-Utgard-VIP:Rack-Chopper")
#epicsEnvSet(R, "PDU")
#epicsEnvSet(TOP, "$(E3_CMD_TOP)")
epicsEnvSet("SNMP_CMD_TOP", "$(raritanpdu5260r_DIR)")
#epicsEnvSet("MIBDIRS", "/usr/share/snmp/mibs:$(raritanpdu5260r_DIR)")
epicsEnvSet(LOCATION, "Utgard: 172.30.244.148")
epicsEnvSet(IPADDR,   "172.30.244.148")
epicsEnvSet(IOCNAME,  "utg-raritanpdu-002")
epicsEnvSet(MIB, "/home/iocuser/.conda/envs/$(IOCNAME)")

##Load IOCSH
iocshLoad("$(raritanpdu5260r_DIR)raritan-px3-5260r.iocsh" "PDU_IP=$(IPADDR),IOC=$(IOCNAME),MIB_HOME=$(MIB)"
#devSnmpSetSnmpVersion("IPADDR","SNMP_VERSION_2c")
#dbLoadRecords("raritan-PX3-5260R-ess.db", "PREFIX=$(IOCNAME):, PDU_IP=$(IPADDR)")


iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

iocInit()

#dbl > "$(TOP)/$(IOC)_PVs.list"
